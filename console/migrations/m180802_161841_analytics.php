<?php

use yii\db\Migration;

/**
 * Class m180802_161841_analytics
 */
class m180802_161841_analytics extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('analytics', [
            'id' => $this->primaryKey(),

            'ip' => $this->string(50),

            'visits' => $this->integer(),
            'downloaded_cv' => $this->boolean(),
            'viewed_cv' => $this->boolean(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('analytics');
    }
}
