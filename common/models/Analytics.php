<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "analytics".
 *
 * @property int $id
 * @property string $ip
 * @property int $visits
 * @property int $downloaded_cv
 * @property int $viewed_cv
 * @property int $created_at
 * @property int $updated_at
 */
class Analytics extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'analytics';
    }

    private static function newOrByIp(string $ip): Analytics
    {
        $query = self::find()->where(['ip' => $ip]);
        if ($query->count() > 0) {
            return $query->one();
        }
        return new self(['ip' => $ip]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['visits'], 'integer'],
            [['downloaded_cv', 'viewed_cv'], 'boolean'],
            [['ip'], 'string', 'max' => 50],
        ];
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'ip' => 'IP',
            'visits' => 'Visits',
            'downloaded_cv' => 'Downloaded CV',
            'viewed_cv' => 'Viewed CV',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function newRequest(): void
    {
        try {
            $route = Yii::$app->requestedRoute;
            $ip = Yii::$app->request->getUserIP();
            $model = self::newOrByIp($ip);
            $model->ip = Yii::$app->request->getUserIP();
            ++$model->visits;
            $model->viewed_cv = $model->viewed_cv ? true : $route === 'site/cv';
            $model->downloaded_cv = $model->downloaded_cv ? true : $route === 'site/download';
            if (!$model->save()) {
                \Yii::error('Could not save model ' . VarDumper::dumpAsString($model->errors));
            }
        } catch (\Exception $e) {
            \Yii::error('There was an issue processing analytics (afterRender) event ' . $e->getMessage());
        }
    }
}
