.PHONY: help clean
default: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: ## Clean up the docker instances
	cd docker && docker-compose -f docker-compose.yml -f docker-compose-dev.yml down

build: ## Attempts to build the docker instances (Images are ignored)
	cd docker && docker-compose -f docker-compose.yml build

build-dev: ## Attempts to build the docker instances (Images are ignored)
	cd docker && docker-compose -f docker-compose.yml -f docker-compose-dev.yml build

restart: ## Restart instances
	cd docker && \
	docker-compose -f docker-compose.yml  restart

deploy: build ## Deploy docker instances with production parameters
	cd docker && \
	docker-compose -f docker-compose.yml up -d --remove-orphans && \
	docker-compose -f docker-compose.yml exec php composer install --no-dev && \
	docker-compose -f docker-compose.yml exec php php init --env=Production --overwrite=No --interactive=0

deploy-dev: build-dev ## Deploy docker instances with development parameters (includes docker-compose-dev.yml)
	cd docker && \
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d --remove-orphans && \
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml exec php composer install && \
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml exec php php init --env=Development --overwrite=No --interactive=0

test: ## Tests with PHP instance
	cd docker && \
	docker-compose -f docker-compose.yml exec php php yii_test migrate --interactive=0 && \
	docker-compose -f docker-compose.yml exec php php vendor/bin/codecept build && \
	docker-compose -f docker-compose.yml exec php php vendor/bin/codecept run

migrate: ## Uses PHP Docker instance to initiate yii migrate
	cd docker && docker-compose -f docker-compose.yml -f docker-compose-dev.yml exec php php yii migrate --interactive=0

migrate-test: ## Uses PHP Docker instance to initiate yii migrate
	cd docker && docker-compose -f docker-compose.yml -f docker-compose-dev.yml exec php php yii_test migrate --interactive=0

migrate-redo: ## Redos the migration
	cd docker && \
	docker-compose -f docker-compose.yml exec php php yii migrate/redo --interactive=0

command: ## Runs a Yii command
	cd docker && \
	docker-compose -f docker-compose.yml exec php php yii $(filter-out $@,$(MAKECMDGOALS))

migrate-create: ## Creates migration
	cd docker && \
	docker-compose -f docker-compose.yml exec php php yii migrate/create $(filter-out $@,$(MAKECMDGOALS)) --interactive=0


bash: ## Enters a PHP instance of Bash
	cd docker && \
	docker-compose -f docker-compose.yml exec php bash

%:
	@: