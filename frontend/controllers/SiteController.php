<?php

namespace frontend\controllers;

use Mpdf\Mpdf;
use Mpdf\Output\Destination;
use Yii;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\HttpException;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
                'layout' => 'error',
            ],
        ];
    }

    public function actionIndex(): string
    {
        return $this->render('index');
    }

    public function actionSkills(): string
    {
        return $this->render('skills');
    }

    public function actionProjects(): string
    {
        return $this->render('projects');
    }

    public function actionContact(): string
    {
        return $this->render('contact');
    }

    public function actionExperience(): string
    {
        return $this->render('experience');
    }

    public function actionFaq(): string
    {
        return $this->render('faq');
    }

    public function actionCv(): string
    {
        $this->layout = 'cv';
        return $this->render('cv');
    }

    /**
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDownload(): \yii\web\Response
    {
        try {
            $mPDF = new Mpdf(['tempDir' => Yii::getAlias('@frontend/runtime/mpdf')]);
            $this->layout = 'cv';
            $view = $this->render('cv');
            $mPDF->WriteHTML($view);
            return Yii::$app->response->sendContentAsFile(
                $mPDF->Output('luka-camernik-cv.pdf', Destination::STRING_RETURN),
                'luka-camernik-cv.pdf'
            );
        } catch (\Exception $e) {
            throw new HttpException('503', 'There was an issue generating PDF file, please try back later');
        }
    }
}
