<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

// This variables repeat within the meta tags since some of them are for Facebook, OpenGraph and twitter cards.
$metaDescription = 'Portfolio and CV of Luka Dobrota Camernik.';
$metaTitle = 'PHP Backend Developer';
$metaImage = 'https://luka.camernik.com/img/luka.jpg';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php /* META */ ?>
    <meta name="description" content="<?= $metaDescription ?>"/>
    <meta property="og:locale" content="en_GB"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://luka.camernik.com"/>
    <meta property="og:title" content="<?= $metaTitle ?>"/>
    <meta property="og:description" content="<?= $metaDescription ?>"/>
    <meta property="og:image" content="<?= $metaImage ?>"/>
    <meta property="og:image:width" content="288"/>
    <meta property="og:image:height" content="294"/>
    <?php /* END OF META */ ?>
    <?php /* FAVICON */ ?>
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/favicon/manifest.json">
    <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <?php /* END OF FAVICON */ ?>

    <?php $this->head() ?>
</head>
<body data-sa-theme="1">
<?php $this->beginBody() ?>

    <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
