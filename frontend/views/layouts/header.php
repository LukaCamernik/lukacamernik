<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;

?>

<div class="page-loader">
    <div class="page-loader__spinner">
        <svg viewBox="25 25 50 50">
            <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
    </div>
</div>

<header class="header">
    <div class="navigation-trigger hidden-xl-up" data-sa-action="aside-open" data-sa-target=".sidebar">
        <i class="zmdi zmdi-menu"></i>
    </div>

    <div class="logo hidden-sm-down">
        <h1><a href="<?= Url::to(['site/index']) ?>">Luka Dobrota Camernik</a></h1>
    </div>
    <ul class="top-nav">
        <li class="hidden-xl-up"><a href="" data-sa-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

        <li class="dropdown hidden-xs-down">
            <a href="" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>

            <div class="dropdown-menu dropdown-menu-right">
                <a href="" class="dropdown-item" data-sa-action="fullscreen">Fullscreen</a>
                <a href="" class="dropdown-item" data-sa-action="clear_local_storage">Clear Local Storage</a>
            </div>
        </li>

        <li class="hidden-xs-down">
            <a href="" class="top-nav__themes" data-sa-action="aside-open" data-sa-target=".themes"><i
                        class="zmdi zmdi-palette"></i></a>
        </li>
    </ul>
</header>
