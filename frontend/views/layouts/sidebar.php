<?php

/* @var $this \yii\web\View */

use yii\helpers\Url;

$current = Yii::$app->requestedRoute;
?>

<aside class="sidebar">
    <div class="scrollbar-inner">

        <div class="user">
            <div class="user__info">
                <img class="user__img" src="/img/luka.jpg" alt="">
                <div>
                    <div class="user__name">Luka Dobrota Camernik</div>
                    <div class="user__email">luka.camernik@gmail.com</div>
                </div>
            </div>
        </div>

        <ul class="navigation">
            <li class="<?= $current === 'site/index' ? 'navigation__active' : '' ?>">
                <a href="<?= Url::to(['site/index']) ?>">
                    <i class="zmdi zmdi-home"></i> Profile
                </a>
            </li>
            <li class="<?= $current === 'site/skills' ? 'navigation__active' : '' ?>">
                <a href="<?= Url::to(['site/skills']) ?>">
                    <i class="zmdi zmdi-chart"></i> Skills
                </a>
            </li>
            <li class="<?= $current === 'site/projects' ? 'navigation__active' : '' ?>">
                <a href="<?= Url::to(['site/projects']) ?>">
                    <i class="zmdi zmdi-folder"></i> Projects
                </a>
            <li class="<?= $current === 'site/contact' ? 'navigation__active' : '' ?>">
                <a href="<?= Url::to(['site/contact']) ?>">
                    <i class="zmdi zmdi-email"></i> Contact
                </a>
            <li class="<?= $current === 'site/experience' ? 'navigation__active' : '' ?>">
                <a href="<?= Url::to(['site/experience']) ?>">
                    <i class="zmdi zmdi-globe"></i> Experiences
                </a>
            <li>
            <li class="<?= $current === 'site/faq' ? 'navigation__active' : '' ?>">
                <a href="<?= Url::to(['site/faq']) ?>">
                    <i class="zmdi zmdi-collection-bookmark"></i> FAQ
                </a>
            <li>
            <li>
                <a href="<?= Url::to(['site/cv']) ?>">
                    <i class="zmdi zmdi-view-list"></i> View CV
                </a>
            <li>
            <li>
                <a href="<?= Url::to(['site/download']) ?>">
                    <i class="zmdi zmdi-download"></i> Download CV
                </a>
            <li>
                <?php /*
            <li class="navigation__sub navigation__sub--active navigation__sub--toggled">
                <a href=""><i class="zmdi zmdi-collection-item"></i> Sample Pages</a>
                <ul>
                    <li class="navigation__active"><a href="profile-about.html">Profile</a></li>
                    <li class="navigation__active"><a href="messages.html">Messages</a></li>
                    <li class="@@contactsactive"><a href="contacts.html">Contacts</a></li>
                    <li class="@@newcontactsactive"><a href="new-contact.html">New Contact</a></li>
                    <li class="@@groupsactive"><a href="groups.html">Groups</a></li>
                    <li class="@@pricingtablesactive"><a href="pricing-tables.html">Pricing Tables</a></li>
                    <li class="@@invoiceactive"><a href="invoice.html">Invoice</a></li>
                    <li class="@@notesactive"><a href="notes.html">Notes</a></li>
                    <li class="@@searchresultsactive"><a href="search-results.html">Search Results</a></li>
                    <li class="@@issuesactive"><a href="issue-tracker.html">Issues Tracker</a></li>
                    <li class="@@faqactive"><a href="faq.html">FAQ</a></li>
                    <li class="@@teamactive"><a href="team.html">Team</a></li>
                    <li class="@@blogactive"><a href="blog.html">Blog</a></li>
                    <li class="@@blogdetailactive"><a href="blog-detail.html">Blog Detail</a></li>
                    <li class="@@qaactive"><a href="questions-answers.html">Questions & Answers</a></li>
                    <li class="@@qadetailactive"><a href="questions-answers-details.html">Questions & Answers
                            Details</a></li>
                    <li class="@@loginactive"><a href="login.html">Login & SignUp</a></li>
                    <li class="@@lockscreenactive"><a href="lockscreen.html">Lockscreen</a></li>
                    <li class="@@lockscreenactive"><a href="404.html">404</a></li>
                    <li class="@@emptyactive"><a href="empty.html">Empty Page</a></li>
                </ul>
            </li>
             */ ?>
        </ul>
    </div>
</aside>

<div class="themes">
    <div class="scrollbar-inner">
        <a href="" class="themes__item" data-sa-value="1"><img src="/img/bg/1.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="2"><img src="/img/bg/2.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="3"><img src="/img/bg/3.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="4"><img src="/img/bg/4.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="5"><img src="/img/bg/5.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="6"><img src="/img/bg/6.jpg" alt=""></a>
        <a href="" class="themes__item active" data-sa-value="7"><img src="/img/bg/7.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="8"><img src="/img/bg/8.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="9"><img src="/img/bg/9.jpg" alt=""></a>
        <a href="" class="themes__item" data-sa-value="10"><img src="/img/bg/10.jpg" alt=""></a>
    </div>
</div>

