<?php

/* @var $this \yii\web\View */

$this->title = 'About | Luka Dobrota Camernik';
?>
<div class="content__inner content__inner--sm">

    <?= $this->render('_profile'); ?>

    <div class="card profile">
        <div class="profile__img">
            <img src="/img/luka-big.jpg" alt="">
        </div>

        <div class="profile__info">
            <p>Backend developer with more than 7 years experience in PHP development</p>

            <ul class="icon-list">
                <li><i class="zmdi zmdi-phone"></i> +49 1578 7243 732</li>
                <li><i class="zmdi zmdi-email"></i> luka.camernik@gmail.com</li>
                <li>

                    <i class="zmdi zmdi-linkedin"></i>
                    <a href="https://www.linkedin.com/in/luka-camernik-97697370/">Luka Camernik
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h4 class="card-body__title mb-4">About Luka Dobrota Camernik</h4>
            <p>
                A highly resourceful, innovative, and competent PHP developer with extensive experience in the layout,
                design and coding of websites specifically in PHP. Possessing considerable knowledge of the development
                of web applications and scripts using PHP programming language and MySQL & SQL Server databases.
                Experienced in developing applications and solutions for a wide range of clients and having the
                enthusiasm and ambition to complete projects to the highest standard. Having worked as QA Tester I have
                a unique perspective on development process.
            </p>
        </div>
    </div>
</div>
