<?php

/* @var $this \yii\web\View */
/* @var $show bool */

$current = Yii::$app->requestedRoute;
?>
<header class="content__title">
    <h1>Luka Dobrota Camernik</h1>
    <small>Backend Developer, Berlin, Germany</small>
</header>
