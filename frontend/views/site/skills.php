<?php

/* @var $this \yii\web\View */

$this->title = 'Skills | Luka Dobrota Camernik';
?>
<div class="content__inner content__inner--sm">

    <?= $this->render('_profile'); ?>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Skills</h4>
            <h6 class="card-subtitle">Ratings are personal opionions</h6>
        </div>

        <div class="listview">
            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">PHP <span class="pull-right">93%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-success" style="width: 93%" aria-valuenow="93" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">HTML5 <span class="pull-right">90%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-warning" style="width: 90%" aria-valuenow="90" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">JavaScript <span class="pull-right">86%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-primary" style="width: 86%" aria-valuenow="86" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">CSS3 <span class="pull-right">80%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-info" style="width: 80%" aria-valuenow="80" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">Docker <span class="pull-right">75%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-danger" style="width: 75%" aria-valuenow="75" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">Golang <span class="pull-right">65%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-danger" style="width: 65%" aria-valuenow="65" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>

            <div class="listview__item">
                <div class="listview__content">
                    <div class="listview__heading">Python <span class="pull-right">65%</span></div>

                    <div class="progress mt-2">
                        <div class="progress-bar bg-danger" style="width: 65%" aria-valuenow="65" aria-valuemin="0"
                             aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
