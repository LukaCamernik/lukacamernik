<?php

/* @var $this \yii\web\View */

$this->title = 'Work Experience | Luka Dobrota Camernik';
?>
<style>
    [data-columns]::before {
        display: block;
        visibility: hidden;
        position: absolute;
        font-size: 1px;
    }

    .card-img-top {
        background-color: white;
        height: 286px;
        width: auto;
        object-fit: cover;
    }

    .card {
        min-height: 510px;
    }
</style>
<div class="content__inner content__inner--sm">

    <?= $this->render('_profile'); ?>

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <img class="card-img-top"
                     src="/img/moeller-ventures.png" alt="Möller Ventures GmbH">
                <div class="card-body">
                    <h4 class="card-title">Möller Ventures GmbH</h4>
                    <h6 class="card-subtitle">Senior Backend Developer</h6>
                    <h6 class="card-subtitle">February 2016 - February 2018</h6>
                    <p>
                        Development of multiple projects started by Möller Ventures GmbH. In charge of backend
                        development and maintenance of multiple projects.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <img class="card-img-top"
                     src="/img/movinga.png" alt="Movinga GmbH">
                <div class="card-body">
                    <h4 class="card-title">Movinga GmbH</h4>
                    <h6 class="card-subtitle">Senior Backend Developer</h6>
                    <h6 class="card-subtitle">November 2015 - February 2016</h6>
                    <p>
                        Backend developer in Symfony 2 framework, working on in-house tools (SalesForce connection and
                        other tools).
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <img class="card-img-top"
                     src="/img/flexperto.png" alt="Flexperto GmbH">
                <div class="card-body">
                    <h4 class="card-title">Flexperto GmbH</h4>
                    <h6 class="card-subtitle">Senior Backend Developer</h6>
                    <h6 class="card-subtitle">March 2014 - November 2015</h6>
                    <p>
                        Responsible for development for core business code flexperto.com in Yii2 framework
                        (previous Yii) and integrating project specific requirements.
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <img class="card-img-top" style="object-fit: contain;"
                     src="/img/el.svg" alt="Édition Lingerie">
                <div class="card-body">
                    <h4 class="card-title">Édition Lingerie</h4>
                    <h6 class="card-subtitle">QA Tester</h6>
                    <h6 class="card-subtitle">January 2014 - March 2014</h6>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <img class="card-img-top"
                     src="/img/classified.png" alt="E-Prodamo D.O.O">
                <div class="card-body">
                    <h4 class="card-title">E-Prodamo D.O.O</h4>
                    <h6 class="card-subtitle">Junior Developer</h6>
                    <h6 class="card-subtitle">2010 - 2012</h6>
                    <p>
                        Development of custom classified solution (PHP) and application (Visual Basic).
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
