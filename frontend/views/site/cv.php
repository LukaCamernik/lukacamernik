<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Curriculum Vitae - Luka Dobrota Camernik';
$phone = '+49 1578 7243 732';
?>
<div id="cv">
    <div class="mainDetails">
        <div id="headshot">
            <img src="/img/luka.jpg" alt="Luka Dobrota Camernik"/>
        </div>

        <div id="name">
            <h1>Luka Dobrota Camernik</h1>
            <h2>PHP Backend Developer</h2>
        </div>

        <div id="contactDetails">
            <ul>
                <li class="pdf-download"><a href="<?= Url::to(['site/download']); ?>"> Download PDF</a></li>
                <li>E-Mail: <a href="mailto:luka.camernik@gmail.com" target="_blank">luka.camernik@gmail.com</a></li>
                <li>Website: <a href="https://luka.camernik.com">https://luka.camernik.com</a></li>
                <li>Mobile: <?= $phone ?></li>
            </ul>
        </div>
        <div class="clear"></div>
    </div>

    <div id="mainArea">
        <section>
            <article>
                <div class="sectionTitle">
                    <h1>Personal Profile</h1>
                </div>

                <div class="sectionContent">
                    <p>
                        A highly resourceful, innovative, and competent PHP developer with extensive experience in the
                        design and coding of websites specifically in PHP. Possessing considerable knowledge of
                        the development of web applications and scripts using PHP programming language and MySQL & SQL
                        Server databases. Experienced in developing applications and solutions for a wide range of
                        clients and having the enthusiasm and ambition to complete projects to the highest standard.
                    </p>
                </div>
            </article>
            <div class="clear"></div>
        </section>


        <section>
            <div class="sectionTitle">
                <h1>Work Experience</h1>
            </div>

            <div class="sectionContent">
                <article>
                    <h3>Möller Ventures GmbH - Backend Developer</h3>
                    <p class="subDetails">February 2016 - February 2018 - Berlin, Germany</p>
                    <p>
                        Development of multiple projects started by Möller Ventures GmbH.
                        In charge of backend development and maintenance of multiple projects.
                    </p>
                </article>

                <article>
                    <h3>Movinga GmbH - Backend Developer</h3>
                    <p class="subDetails">November 2015 - February 2016 - Berlin, Germany</p>
                    <p>
                        Backend developer in Symfony 2 framework, working on in-house tools (SalesForce connection and
                        other tools).
                    </p>
                </article>

                <article>
                    <h3>Flexperto GmbH - Backend Developer</h3>
                    <p class="subDetails">March 2014 - November 2015 - Berlin, Germany</p>
                    <p>
                        Development, enhancement and maintenance of framework and clients applications.
                        Worked with PHP (Yii and Yii2) and NodeJS backend as micro-service for Chat and WebRTC Video
                        chat.
                    </p>
                </article>

                <article>
                    <h3>Édition Lingerie - QA Tester</h3>
                    <p class="subDetails">January 2014 - March 2014 - Berlin, Germany</p>
                    <span>&nbsp;</span>
                </article>
                <article>
                    <h3>E-Prodamo d.o.o - Backend Developer</h3>
                    <p class="subDetails">March 2010 - February 2012 - Berlin, Germany</p>
                    <p>
                        Development of custom classified solution and application in Visual Basic.
                    </p>
                </article>
            </div>
            <div class="clear"></div>
        </section>

        <section>
            <div class="sectionTitle">
                <h1>Key Skills</h1>
            </div>

            <div class="sectionContent">
                <ul class="keySkills">
                    <li>PHP</li>
                    <li>PHP Frameworks</li>
                    <li>MySQL</li>
                    <li>HTML5</li>
                    <li>CSS</li>
                    <li>GIT</li>
                    <li>Javascript</li>
                    <li>GoLang</li>
                    <li>Python</li>
                </ul>
            </div>
            <div class="clear"></div>
        </section>
        <section>
            <div class="sectionTitle">
                <h1>Hobbies</h1>
            </div>

            <div class="sectionContent">
                <article>
                    <h3>Go (ancient board game)</h3>
                    <p class="subDetails">
                        Played in tournaments and reached 3 dan rank.
                    </p>
                </article>
                <article>
                    <h3>Video Games</h3>
                    <p class="subDetails">
                        Strive to get better at any thing I set my mind to, including video games and have attended a
                        tournament at a young age.
                    </p>
                </article>
                <article>
                    <h3>Programming</h3>
                    <p class="subDetails">
                        At young age learned PHP to create my own website and Visual Basic applications for whatever I
                        needed.
                    </p>
                </article>
            </div>
            <div class="clear"></div>
        </section>
    </div>
</div>
