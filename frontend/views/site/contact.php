<?php

/* @var $this \yii\web\View */

$this->title = 'Contact | Luka Dobrota Camernik';
?>
<div class="content__inner content__inner--sm">

    <?= $this->render('_profile'); ?>

    <div class="card widget-contacts">
        <div class="card-body">
            <h4 class="card-title">Contact Information</h4>
            <h6 class="card-subtitle">
                I prefer to use E-Mail since it allows asynchronous communication, but phone call is possible too.
            </h6>

            <ul class="icon-list">
                <li><i class="zmdi zmdi-phone"></i> +49 1578 7243 732</li>
                <li><i class="zmdi zmdi-email"></i> luka.camernik@gmail.com</li>
                <li><i class="zmdi zmdi-pin"></i>
                    <address>
                        Berlin, Germany
                    </address>
                </li>
            </ul>
        </div>

        <a href="/img/map.png" class="widget-contacts__map">
            <img src="/img/map.png" alt="">
        </a>
    </div>
</div>
