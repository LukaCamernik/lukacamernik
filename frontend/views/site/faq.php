<?php

use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Frequently Asked Questions | Luka Dobrota Camernik';
?>

<div class="content__inner content__inner--sm">

    <?= $this->render('_profile'); ?>
    <div class="card">
        <div class="card-header">What are you looking for in a new position?</div>
        <div class="card-body">
            <p class="card-text">
                I am most of all looking for challenging and fun environment to work in.
            </p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">Are you a full stack developer?</div>
        <div class="card-body">
            <p class="card-text">
                I am backend developer,
                I can work with full stack (LAMP - Linux, Apache, MySQL, PHP or other similar
                stacks).
                <br>
                I also work with fixing/creating from existing HTML and CSS templates,
                create "DevOps" configurations (Docker or bare-metal servers).
                <br>

            </p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">What technologies do you work with?</div>
        <div class="card-body">
            <p class="card-text">
                Backend: My favorite language is PHP, although Golang is slowly becoming close second.
                I always believed that each language has best use cases.
                <br>
                I like to keep up-to-date with PHP, Golang, Python
                and Java and I like to keep an eye on others to see what is the direction they are taking it.
                <br>
                Database: my favorite is MySQL although I have used SQLite in the past when MySQL would have been
                overkill.
                <br>
                Frontend: I prefer Vanilla HTML5, CSS3 and Javascript.
                <br>
                Other tools I use: Docker, IntelliJ Suite (PHPStorm, Goland, etc..), PHPUnit, Codeception, Linux.
            </p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">Would you relocate?</div>
        <div class="card-body">
            <p class="card-text">
                Currently I am not open for relocation, I only accept Berlin, Germany offers for now.
            </p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">Do you have GitHub?</div>
        <div class="card-body">
            <p class="card-text">
                I do not use GitHub, I use Bitbucket since it has free private projects and Github charges for that.
                <br>
                You can view all my public projects on the left under "Projects" or
                <a href="https://bitbucket.org/LukaCamernik">here</a>
            </p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">Are you open for phone call?</div>
        <div class="card-body">
            <p class="card-text">
                I prefer to do things over E-Mail, it's easier to track what happened and when it happened.
                <br>
                It's also asynchronous, so I can work and still answer questions when I find the time.
                <br>
                If you think Phone call is more beneficial or there is something you can't do over E-Mail,
                just send me an E-Mail when you want the phone call to happen (Monday - Friday between 12:00 - 19:00)
            </p>
        </div>
    </div>
    <div class="card">
        <div class="card-header">Can I get your CV?</div>
        <div class="card-body">
            <p class="card-text">
                Sure you can
                <a href="<?= Url::to(['site/download']) ?>">download it here</a> (if you click it, it will start
                download and remain on this page)
                or
                <a href="<?= Url::to(['site/cv']) ?>">view it here</a>.
            </p>
        </div>
    </div>
</div>
