<?php

/* @var $this \yii\web\View */

$this->title = 'Projects | Luka Dobrota Camernik';
?>
<div class="content__inner content__inner--sm">

    <?= $this->render('_profile'); ?>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Recent Public Projects</h4>
        </div>
        <div class="listview listview--hover">
            <a target="_blank" href="https://bitbucket.org/LukaCamernik/LukaCamernik" class="listview__item">
                <img src="/img/yii2.png" class="listview__img" alt="">

                <div class="listview__content">
                    <div class="listview__heading">Personal Web (CV)</div>
                    <p>
                        This CV is also public project so you can inspect my coding style (Minimalist since it's simple
                        web page)
                    </p>
                </div>
            </a>
            <a href="https://bitbucket.org/LukaCamernik/docker-runner" target="_blank" class="listview__item">
                <img src="/img/gopher.png" class="listview__img" alt="">

                <div class="listview__content">
                    <div class="listview__heading">Docker Runner</div>
                    <p title="Docker service start up sequence is broken when you are mounting external disks,
                     which are slow to load.
                      This command line program fixes that by waiting for disks and then starts docker">
                        Docker service start up sequence is broken when you are mounting external
                        disks, which are slow to load. This command line program fixes that by waiting for disks and
                        then starts docker
                    </p>
                </div>
            </a>
            <a href="https://bitbucket.org/LukaCamernik/php-docker" target="_blank" class="listview__item">
                <img src="/img/docker.png" class="listview__img" alt="">

                <div class="listview__content">
                    <div class="listview__heading">PHP FPM (Docker Image)</div>
                    <p>
                        Build files for php-fpm with docker, published on Docker Hub
                    </p>
                </div>
            </a>
            <a target="_blank" href="https://bitbucket.org/LukaCamernik/notifier" class="listview__item">
                <img src="/img/python.png" class="listview__img" alt="">

                <div class="listview__content">
                    <div class="listview__heading">Bitbucket Notifier</div>
                    <p>
                        If you want to see any commit made to any followed repository. Made with Python3 and PyQT5.
                    </p>
                </div>
            </a>
            <a target="_blank" href="https://bitbucket.org/LukaCamernik/yii2-app-advanced" class="listview__item">
                <img src="/img/yii2.png" class="listview__img" alt="">

                <div class="listview__content">
                    <div class="listview__heading">Yii2 Advance Template</div>
                    <p>
                        Bootstrap any project with Yii2, changes from original made with sane defaults.
                    </p>
                </div>
            </a>
            <a href="https://bitbucket.org/LukaCamernik" class="view-more">View All Public Projects</a>
        </div>
    </div>
</div>
