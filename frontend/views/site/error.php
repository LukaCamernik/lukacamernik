<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<section class="error">
    <div class="error__inner">
        <h1><?= $exception->statusCode ?></h1>
        <h2><?= nl2br(Html::encode($message)) ?></h2>
        <p>
            Please return <a href="<?= Url::to(['site/index']); ?>">home</a> and try again.
        </p>
    </div>
</section>
