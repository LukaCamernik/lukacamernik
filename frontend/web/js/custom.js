$body = $("body");
if (b = localStorage.getItem('selected_theme')) {
    $body.attr("data-sa-theme", b);
}
$(document).on("click", ".themes__item", function () {
    localStorage.setItem('selected_theme', $(this).data("sa-value"));
});

$body.on("click", "[data-sa-action]", function () {
    if ($(this).data("sa-action") === "clear_local_storage") {
        localStorage.clear();
        window.location.reload(true);
    }
});