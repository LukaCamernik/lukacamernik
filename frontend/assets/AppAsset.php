<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'vendor/material-design-iconic-font/css/material-design-iconic-font.css',
        'vendor/animate.css/animate.min.css',
        'vendor/jquery.scrollbar/jquery.scrollbar.css',
        'css/app.min.css',
    ];
    public $js = [
        'vendor/popper.js/popper.min.js',
        'vendor/bootstrap/bootstrap.min.js',
        'vendor/jquery.scrollbar/jquery.scrollbar.min.js',
        'vendor/jquery-scrollLock/jquery-scrollLock.min.js',
        'js/app.min.js',
        'js/custom.js',
    ];
    public $depends = [
        YiiAsset::class,
    ];
}
